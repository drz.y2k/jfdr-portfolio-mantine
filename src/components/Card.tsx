import {
	IconBrandGithub,
	IconBrandGitlab,
	IconBrandLinkedin,
	IconMail,
	IconBlockquote,
	IconBraces,
} from '@tabler/icons-react';

import {
	Card,
	Text,
	Group,
	ActionIcon,
	createStyles,
	Avatar,
	Accordion,
	Flex,
	Tabs,
	Popover,
	Button,
} from '@mantine/core';

import { useEffect, useState } from 'react';

import { TimeLineExperience } from './TimeLineExperience';
import { ListProjects } from './ListProjects';

const useStyles = createStyles((theme) => ({
	card: {
		backgroundColor:
			theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white,

		width: '500px',
		margin: '30px',
	},

	section: {
		// borderBottom: `1px solid ${
		// 	theme.colorScheme === 'dark'
		// 		? theme.colors.dark[4]
		// 		: theme.colors.gray[3]
		// }`,
		paddingLeft: theme.spacing.md,
		paddingRight: theme.spacing.md,
		// paddingBottom: theme.spacing.md,
	},

	project: {
		margin: '10px',
		padding: '10px',
		border: `2px solid ${
			theme.colorScheme === 'dark'
				? theme.colors.dark[4]
				: theme.colors.gray[3]
		}`,
	},

	like: {
		color: theme.colors.red[6],
	},

	label: {
		// textTransform: 'uppercase',
		fontSize: theme.fontSizes.md,
		fontWeight: 700,
	},

	avatar: {
		border: `2px solid ${
			theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white
		}`,
	},

	containerBackGround: {
		backgroundColor:
			theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.white,
		borderRadius: 10,
		padding: '10px',
		paddingBottom: '20px',
		width: '100%',
		textAlign: 'center',
	},
}));

export function BadgeCard() {
	const { classes, theme } = useStyles();
	const [opened, setOpened] = useState(false);

	function copyMail() {
		setOpened(true);
		navigator.clipboard.writeText('francisco.diosdado@outlook.com');
	}

	useEffect(() => {
		if (opened) {
			setTimeout(() => {
				setOpened(false);
			}, 1000);
		}
	}, [opened]);

	return (
		<Card withBorder radius="md" p="md" className={classes.card}>
			<Card.Section className={classes.section}>
				<Card.Section
					sx={{
						backgroundImage: `url(https://images.unsplash.com/photo-1583484963886-cfe2bff2945f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80)`,
						height: 80,
						backgroundSize: 'cover',
						backgroundPosition: 'center',
					}}
				/>
				<Avatar
					src={'assets/img/avatar-min.jpg'}
					size={130}
					radius={100}
					mx="auto"
					mt={'-70px'}
					className={classes.avatar}
				/>
				<Text align="center" size="xl" mt={'md'} weight={500}>
					Francisco Diosdado
				</Text>
				<Text align="center" size="lg" color="dimmed">
					Full Stack Developer
				</Text>
				<Popover
					width={100}
					opened={opened}
					position="bottom"
					withArrow
					shadow="lg"
				>
					<Popover.Target>
						<Text
							align="center"
							size="sm"
							mt={'0px'}
							color="dimmed"
							style={{
								cursor: 'pointer',
							}}
							onClick={() => copyMail()}
						>
							francisco.diosdado@outlook.com
						</Text>
					</Popover.Target>
					<Popover.Dropdown
						style={{ borderRadius: '10px' }}
						color="teal"
					>
						<Text size="sm" align="center">
							Copied!
						</Text>
					</Popover.Dropdown>
				</Popover>
				{/* <Text align="center" size="sm" mt={'0px'} color="dimmed">
					francisco.diosdado@outlook.com
				</Text> */}
				<Group spacing="sm" position="center" mt="xs" noWrap>
					<ActionIcon
						size="md"
						variant="default"
						radius="sm"
						component="a"
						href="https://github.com/drzz21"
						target={'_blank'}
					>
						<IconBrandGithub size={20} stroke={1.4} />
					</ActionIcon>
					<ActionIcon
						size="md"
						variant="default"
						radius="sm"
						component="a"
						href="https://gitlab.com/drz.y2k"
						target={'_blank'}
					>
						<IconBrandGitlab size={20} stroke={1.4} />
					</ActionIcon>
					<ActionIcon
						size="md"
						variant="default"
						radius="sm"
						component="a"
						href="https://www.linkedin.com/in/drzy2k/"
						target={'_blank'}
					>
						<IconBrandLinkedin size={20} stroke={1.4} />
					</ActionIcon>
					<ActionIcon
						size="md"
						variant="default"
						radius="sm"
						component="a"
						href="mailto:francisco.diosdado@outlook.com"
						target={'_blank'}
					>
						<IconMail size={20} stroke={1.4} />
					</ActionIcon>
				</Group>
			</Card.Section>
			<Card.Section className={classes.section}></Card.Section>

			<Card.Section className={classes.section}>
				<Tabs
					defaultValue="first"
					inverted
					mt={'sm'}
					mb={'sm'}
					variant="pills"
					radius="md"
				>
					<Tabs.List position="center">
						<Tabs.Tab
							value="first"
							icon={<IconBlockquote size={18} />}
						>
							Info
						</Tabs.Tab>
						<Tabs.Tab
							value="second"
							icon={<IconBraces size={18} />}
						>
							Projects
						</Tabs.Tab>
					</Tabs.List>
					<Tabs.Panel value="first" pt="xs">
						<Accordion
							variant="separated"
							defaultValue="customization"
						>
							<Accordion.Item value="customization">
								<Accordion.Control>About Me</Accordion.Control>
								<Accordion.Panel>
									<Flex
										direction={'column'}
										px="md"
										justify={'center'}
										align="center"
									>
										<Text
											// align="center"
											size="md"
											weight={400}
										>
											Web Developer with 4+ years of
											experience, always striving in bring
											creative solutions to complex
											problems in a collaborative
											environment and communication
											oriented.
										</Text>
										<Text
											// align="center"
											size="md"
											weight={400}
										>
											I enjoy the process of learning and
											I like to be part multidisciplinary
											teams to achieve common goals and to
											improve products in order to meet
											business and user needs.
										</Text>
										{/* <Button
											compact
											mt="xs"
											sx={{
												width: '250px',
												[theme.fn.smallerThan('xs')]: {
										   			maxWidth: '100%',
												},




										}}
											component="a"
											href="./assets/cv/cv.pdf"
											download="cv.pdf"
										>
											<Container mr={'xs'}>
												Download resumee
											</Container>

											<IconDownload
												size={19}
												stroke={1.9}
											/>
										</Button> */}
									</Flex>
								</Accordion.Panel>
							</Accordion.Item>

							<Accordion.Item value="flexibility">
								<Accordion.Control>
									Professional Experience
								</Accordion.Control>
								<Accordion.Panel>
									<Flex
										direction={'column'}
										px="md"
										justify={'center'}
										align="center"
										gap={'md'}
									>
										<Text>
											I have been involved in the
											development of mainly web
											applications, using different
											technologies and frameworks, such
											as: Angular, React, Vue, Laravel,
											NodeJS, Express, MySQL, MongoDB,
											Oracle, among others working on both
											front end and back end.
										</Text>
										<Text>
											Currently searching for a team where
											I can grow as a professional and
											contribute with my knowledge and
											experience.
										</Text>
										<Text>
											In the Projects section you can see
											some projects that I have worked on
											as part of my learning process. The
											source code is public and you can
											find it in my GitHub and GitLab
											accounts.
										</Text>
									</Flex>
									{/* <TimeLineExperience /> */}
								</Accordion.Panel>
							</Accordion.Item>
						</Accordion>
					</Tabs.Panel>
					<Tabs.Panel value="second" pt="xs">
						<ListProjects />
					</Tabs.Panel>
				</Tabs>
			</Card.Section>
		</Card>
	);
}
