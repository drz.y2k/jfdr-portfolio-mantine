import {
	Switch,
	Group,
	useMantineColorScheme
} from '@mantine/core';
import { IconSunFilled, IconMoonFilled } from '@tabler/icons-react';

export function SwitchToggle() {
	const { colorScheme, toggleColorScheme } = useMantineColorScheme();

	return (
		<Group position="center" my={30}>
			<Switch
				checked={colorScheme === 'dark'}
				onChange={() => toggleColorScheme()}
				size="lg"
				onLabel={<IconSunFilled color="red" size={25} />}
				offLabel={<IconMoonFilled color="red" size={20} />} 
			/>
		</Group>
	);
}
