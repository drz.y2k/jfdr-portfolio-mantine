import { Text, Timeline, Stack, Flex, List } from '@mantine/core';
import { BadgeSkill } from './BadgeSkill';
import { Skill } from '../enum/Skill';


const obj = [
	{
		title: 'Nagarro (Full Time)',
		date: 'Aug 2022 - Dec 2022',
		duties: [],
		skills: [Skill.ReactJS,Skill.JavaScript,Skill.VBA],
	},
	{
		title: 'Sibyla (Full Time)',
		date: 'Oct 2021 - Jun 2022',
		duties: [],
		skills: [Skill.Angular,Skill.Typescript,Skill.Oracle,Skill.Laravel,Skill.PrimeNg,Skill.GitHub],
	},
	{
		title: 'IPS Paquetería (Temporary Contract)',
		date: 'Sept 2020 - May 2021',
		duties: [],
		skills: [Skill.VueJS,Skill.JavaScript,Skill.Vuetify,Skill.GitHub,Skill.MySQL],
	},
	{
		title: 'Instituto Yurirense A.C. (Internship)',
		date: 'Aug 2019 - Feb 2020',
		duties: [],
		skills: [Skill.VueJS,Skill.JavaScript,Skill.Bootstrap,Skill.GitHub,Skill.MySQL],
	},
];

export const TimeLineExperience = () => {
	return (
		<Timeline active={5} bulletSize={15} lineWidth={3}>
			{obj.map(({ title, date, duties, skills }, index) => (
				<Timeline.Item key={title} title={title}>
					<Text size="xs" mt={4}>
						{date}
					</Text>

					{duties.length > 0 && (
						<List mt={'sm'} size="sm" center spacing="xs">
							{duties.map((duty) => (
								<List.Item key={duty}>{duty}</List.Item>
							))}
						</List>
					)}
					<Stack spacing={'xs'}>
						<Text weight={500} mt="xs">
							Skillset:
						</Text>
						<Flex
							wrap="wrap"
							justify={'flex-start'}
							align={'center'}
							gap="xs"
						>
							{skills.map((skill) => (
								<BadgeSkill key={skill} skill={skill} />
							))}
							{/* <BadgeSkill /> */}
						</Flex>
					</Stack>
				</Timeline.Item>
			))}
		</Timeline>
	);
};
