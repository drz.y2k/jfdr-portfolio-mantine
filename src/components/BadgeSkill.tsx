import { Avatar, Badge, Text } from '@mantine/core';
import { Skill } from '../enum/Skill';

const URL_BASE = './assets/tech-logos/';

export const BadgeSkill: React.FC<{ skill: Skill }> = ({ skill }) => {
	const skills:Record<Skill,string> = {
		[Skill.ReactJS]: 'react-logo.png',
		[Skill.Angular]: 'angular-logo.png',
		[Skill.VueJS]: 'vue-logo.png',
		[Skill.Typescript]: 'typescript-logo.png',
		[Skill.AWS]: 'aws-logo.jpg',
		[Skill.JavaScript]: 'javascript-logo.png',
		[Skill.VBA]: 'vba-logo.jpg',
		[Skill.Oracle]: 'oracle-logo.png',
		[Skill.Laravel]: 'laravel-logo.png',
		[Skill.Bootstrap]: 'bootstrap-logo.png',
		[Skill.Vuetify]: 'vuetify-logo.svg',
		[Skill.PrimeNg]: 'primeng-logo.webp',
		[Skill.GitHub]: 'github-logo.png',
		[Skill.MySQL]: 'mysql-logo.png',
		[Skill.Vite]: 'vite-logo.png',
		[Skill.TailwindCSS]: 'tailwind-logo.png',
		[Skill.Webpack]: 'webpack-logo.png',
		[Skill.Mantine]: 'mantine-logo.png',
		[Skill.Firebase]: 'firebase-logo.png',
		[Skill.ChakraUI]: 'chakra-logo.png',
		[Skill.NodeJS]: 'nodejs-logo.png',
		[Skill.Vercel]: 'vercel-logo.jpg',
		[Skill.Express]: 'express-logo.png',
		[Skill.AWS_S3]: 'aws-s3-logo.jpg',
		[Skill.AWS_Rekognition]: 'aws-rekognition-logo.png',
		[Skill.MongoDB]: 'mongo-logo.png',
		[Skill.AntDesign]: 'ant-design-logo.jpg',
		[Skill.ReactRouter]: 'react-router-logo.png',
		[Skill.Zustand]: 'zustand-logo.png',
		[Skill.WebGI]: 'webgi-logo.svg',
		[Skill.SASS]: 'sass-logo.jpg',
	};

	const avatar = (
		<Avatar
			alt="Avatar for badge"
			size={25}
			mr={5}
			radius="xl"
			src={`${URL_BASE}${skills[skill]}`}
		/>
	);

	return (
		<>
			<Badge
				sx={{ paddingLeft: 0, textTransform: 'none' }}
				size="lg"
				radius="xl"
				leftSection={avatar}
			>
				<Text>{skill}</Text>
			</Badge>
		</>
	);
};
