import {
	Text,
	Button,
	Container,
	Flex,
	Paper,
	Image,
	Pagination,
	Center,
	Badge,
	Stack,
	Popover,
} from '@mantine/core';

import { useState } from 'react';

import { IconCode, IconExternalLink } from '@tabler/icons-react';
import { useMediaQuery } from '@mantine/hooks';

import { Skill } from '../enum/Skill';

import { BadgeSkill } from './BadgeSkill';

type fullStackURL = {
	backend: string;
	frontend: string;
};

interface projects {
	title: string;
	description: string;
	techStack: Skill[];
	image: string;
	url: string | fullStackURL;
	sourceCode: string | fullStackURL;
	page?: number;
	fullStackApp?: boolean;
}

const arrProjects: projects[] = [
	{
		title: 'Golf GTI W12 Sample Site',
		description:
			'This projects is a sample landing page showing information about a specific model and brand of car. It has sections with information, a toggleable side menu, and also a gallery where we can find a 3d version of the car.',
		techStack: [
			Skill.Typescript,
			Skill.ReactJS,
			Skill.Vercel,
			Skill.WebGI,
			Skill.SASS,
		],
		image: './assets/projects/golf_gti.png',
		url: 'https://volkswagen-animation-example.vercel.app/',
		sourceCode: 'https://github.com/drzz21/volkswagen-animation-example',
	},
	{
		title: 'Mocktails-API',
		description:
			'This projects mocktails searcher, the user can search for a mocktail or fetch a random mocktail, the the user can view a new page with the ingredients and more info of how to prepare that mocktail.',
		techStack: [
			Skill.Typescript,
			Skill.Vercel,
			Skill.AntDesign,
			Skill.ReactRouter,
			Skill.Zustand,
			Skill.Vite,
		],
		image: './assets/projects/mocktails-api.png',
		url: 'https://tacos-api-khaki.vercel.app/',
		sourceCode: 'https://gitlab.com/drz.y2k/tacos-api',
	},
	{
		title: 'Dessertify',
		description:
			'This is a Full Stack application, it is a page where the user can load a picture of a dessert, and then using AI the app will tell the user if the loaded picture is or not a dessert, the app can store up to 10 pictures of desserts in the database.',
		techStack: [
			Skill.ReactJS,
			Skill.Vite,
			Skill.Typescript,
			Skill.ChakraUI,
			Skill.NodeJS,
			Skill.Vercel,
			Skill.Express,
			Skill.AWS,
			Skill.AWS_S3,
			Skill.AWS_Rekognition,
			Skill.MongoDB,
		],
		image: './assets/projects/dessertify.png',
		url: {
			frontend: 'https://dessertify-front.vercel.app/',
			backend: 'https://dessertify-back.vercel.app/',
		},
		sourceCode: {
			frontend: 'https://gitlab.com/drz.y2k/dessertify-front',
			backend: 'https://gitlab.com/drz.y2k/dessertify-back',
		},
		fullStackApp: true,
	},
	{
		title: 'This Portfolio',
		description:
			'The current portfolio shows my professional experience, contact information, and some of my projects. It also has a dark and light mode.',
		techStack: [
			Skill.ReactJS,
			Skill.Vite,
			Skill.Typescript,
			Skill.Mantine,
			Skill.Firebase,
		],
		image: './assets/projects/jfdr-portfolio.png',
		url: 'https://jfdr.dev',
		sourceCode: 'https://gitlab.com/drz.y2k/jfdr-portfolio-mantine',
	},
	{
		title: 'PianoChordPedia',
		description:
			'App that shows to the user the selected chord in piano, the user can search an element in a input and the app will show the chord in piano, or can hit the keys and the app will show the name of the selected chord',
		techStack: [
			Skill.ReactJS,
			Skill.TailwindCSS,
			Skill.Vite,
			Skill.Typescript,
		],
		image: './assets/projects/pianochordpedia.png',
		url: 'https://pianochordpedia.vercel.app/',
		sourceCode: 'https://gitlab.com/drz.y2k/pianochordpedia',
	},
	{
		title: 'Rick & Morty Api',
		description:
			'App that shows the characters of the Rick and Morty show using the Rick and Morty API, it was developed with ReactJS and Bootstrap. For the requests to the API, the fetch method was used.',
		techStack: [
			Skill.ReactJS,
			Skill.Bootstrap,
			Skill.Vite,
			Skill.Typescript,
		],
		image: './assets/projects/rickmortyapi.png',
		url: 'https://react-ts-eyyeya.stackblitz.io/',
		sourceCode: 'https://stackblitz.com/edit/react-ts-eyyeya',
	},
	{
		title: 'Password Generator',
		description:
			'I made this app for a virtual contest, the app generates a password with the length that the user wants, and the user can select the type of characters that the password will have.',
		techStack: [Skill.ReactJS, Skill.TailwindCSS],
		image: './assets/projects/password-generator.png',
		url: 'https://hacktoberfest-2022.vercel.app/entry/drzz21',
		sourceCode:
			'https://github.com/midudev/password-generator/tree/main/src/components/drzz21',
	},
	{
		title: 'Tenzies',
		description:
			'Tenzies is a numbers game, in which the user must lock the numbers to the same value, in each round the user can roll the dice and generate new numbers, the user win the game when all the numbers are the same.',
		techStack: [Skill.ReactJS, Skill.Webpack],
		image: './assets/projects/tenzies.png',
		url: 'https://tenzies-umber.vercel.app/',
		sourceCode: 'https://gitlab.com/drz.y2k/tenzies',
	},
];
let page = 1;
let counter = 0;
arrProjects.forEach((el) => {
	el.page = page;
	counter++;
	if (counter == 4) {
		counter = 0;
		page++;
	}
});

const pager = (page: number) => {
	return arrProjects.filter((el) => el.page == page);
};

const numPages = Math.ceil(arrProjects.length / 4);

export const ListProjects = () => {
	const sm = useMediaQuery('(max-width: 500px)');
	const [projects, setProjects] = useState<projects[]>(pager(1));
	return (
		<Flex direction={'column'} gap={'sm'}>
			<Paper withBorder radius="md" p="sm">
				<Text size={'md'} align="center">
					Here is a list of public projects that I have developed as
					part of my learning process.
				</Text>
				<Text size={'sm'} align="center" mt={'xs'}>
					There are currently{' '}
					<Badge color="indigo" radius="md" size="sm">
						{arrProjects.length}
					</Badge>{' '}
					projects listed.
				</Text>
			</Paper>

			{projects.map((project: projects) => (
				<Paper
					key={project.title}
					shadow="xs"
					radius="md"
					p="lg"
					withBorder
				>
					<Flex direction={sm ? 'column' : 'row'}>
						<Image
							width={'100%'}
							fit="contain"
							radius="md"
							src={project.image}
							alt="Random unsplash image"
						/>
						<Container
							m={'sm'}
							sx={{
								display: 'flex',
								flexDirection: 'column',
								gap: '10px',
							}}
						>
							<Text
								size={'lg'}
								fw="bold"
								sx={{ wordBreak: 'break-word' }}
							>
								{project.title}
							</Text>
							{project.fullStackApp &&
								typeof project.url === 'object' && (
									<Popover
										position="bottom"
										withArrow
										shadow="md"
									>
										<Popover.Target>
											<Button
												size="xs"
												compact
												variant="outline"
											>
												<IconCode
													size={18}
													stroke={1.7}
												/>
												Open
											</Button>
										</Popover.Target>
										<Popover.Dropdown>
											<Stack>
												<Button
													size="xs"
													compact
													variant="outline"
													component="a"
													href={project.url.frontend}
													target={'_blank'}
												>
													<IconCode
														size={18}
														stroke={1.7}
													/>
													Front End
												</Button>
												<Button
													size="xs"
													compact
													variant="outline"
													component="a"
													href={project.url.backend}
													target={'_blank'}
												>
													<IconCode
														size={18}
														stroke={1.7}
													/>
													Back End
												</Button>
											</Stack>
										</Popover.Dropdown>
									</Popover>
								)}
							{!project.fullStackApp && (
								<Button
									size="xs"
									compact
									variant="outline"
									component="a"
									href={project.url as string}
									target={'_blank'}
								>
									<IconExternalLink size={18} stroke={1.7} />
									Open
								</Button>
							)}
							{project.fullStackApp &&
								typeof project.sourceCode === 'object' && (
									<Popover
										position="bottom"
										withArrow
										shadow="md"
									>
										<Popover.Target>
											<Button
												size="xs"
												compact
												variant="outline"
											>
												<IconCode
													size={18}
													stroke={1.7}
												/>
												Source Code
											</Button>
										</Popover.Target>
										<Popover.Dropdown>
											<Stack>
												<Button
													size="xs"
													compact
													variant="outline"
													component="a"
													href={
														project.sourceCode
															.frontend
													}
													target={'_blank'}
												>
													<IconCode
														size={18}
														stroke={1.7}
													/>
													Front End
												</Button>
												<Button
													size="xs"
													compact
													variant="outline"
													component="a"
													href={
														project.sourceCode
															.backend
													}
													target={'_blank'}
												>
													<IconCode
														size={18}
														stroke={1.7}
													/>
													Back End
												</Button>
											</Stack>
										</Popover.Dropdown>
									</Popover>
								)}
							{!project.fullStackApp && (
								<Button
									size="xs"
									compact
									variant="outline"
									component="a"
									href={project.sourceCode as string}
									target={'_blank'}
								>
									<IconCode size={18} stroke={1.7} />
									Source Code
								</Button>
							)}
						</Container>
					</Flex>
					<Text size={'sm'} mt="xs" color={'dimmed'}>
						{project.description}
					</Text>
					<Text size={'md'} fw="400" m={'xs'}>
						Tech Stack:
					</Text>
					<Flex gap={'sm'} wrap="wrap">
						{project.techStack.map((skill) => (
							<BadgeSkill key={skill} skill={skill} />
						))}
					</Flex>
				</Paper>
			))}
			<Center inline>
				<Pagination
					total={numPages}
					onChange={(v) => setProjects(pager(v))}
				/>
			</Center>
		</Flex>
	);
};
