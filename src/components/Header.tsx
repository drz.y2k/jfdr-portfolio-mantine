import { createStyles, Header, Group, Burger, Container } from '@mantine/core';
import { useDisclosure } from '@mantine/hooks';
import { SwitchToggle } from './SwitchToggle';

import { IconBrandAppleArcade } from '@tabler/icons-react';

const useStyles = createStyles((theme) => ({
	inner: {
		height: 56,
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
	},

	flexRow: {
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		fontWeight: 500,

		'& *': {
			margin: '5px',
		},
	},
}));

interface HeaderSearchProps {
	links: {
		link: string;
		label: string;
		links: { link: string; label: string }[];
	}[];
}

export function HeaderMenu() {
	const { classes } = useStyles();

	return (
		<Header height={56} sx={{ position: 'sticky', top: 0, width: '100%',height:'56px'}}>
			<Container>
				<div className={classes.inner}>
					<div className={classes.flexRow}>
						JFDR.DEV
						<IconBrandAppleArcade size={28} />
					</div>

					<SwitchToggle />
				</div>
			</Container>
		</Header>
	);
}
