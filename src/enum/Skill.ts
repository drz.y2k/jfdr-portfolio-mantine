export enum Skill {
	ReactJS = 'ReactJS',
	Typescript = 'Typescript',
	JavaScript = 'JavaScript',
	Angular = 'Angular',
	VueJS = 'VueJS',
	AWS = 'AWS',
	VBA = 'VBA',
	Oracle = 'Oracle',
	Laravel = 'Laravel',
	Bootstrap= 'Bootstrap',
	Vuetify = 'Vuetify',
	PrimeNg= 'PrimeNg',
	GitHub = 'GitHub',
	MySQL = 'MySQL',
	Vite = 'Vite',
	TailwindCSS = 'TailwindCSS',
	Webpack = 'Webpack',
	Mantine= 'Mantine',
	Firebase = 'Firebase',
	ChakraUI = 'ChakraUI',
	NodeJS = 'NodeJS',
	Vercel= 'Vercel',
	Express= 'Express',
	AWS_S3 = 'AWS S3',
	AWS_Rekognition = 'AWS Rekognition',
	MongoDB= 'MongoDB',
	AntDesign='AntDesign',
	ReactRouter='ReactRouter',
	Zustand='Zustand',
	WebGI='WebGI',
	SASS='SASS'
}
