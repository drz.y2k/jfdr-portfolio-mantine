import { HeaderMenu } from './components/Header';
import { BadgeCard } from './components/Card';
import { useState } from 'react';

import {
	MantineProvider,
	ColorSchemeProvider,
	ColorScheme,
	Flex,
	Container,
} from '@mantine/core';

export default function App() {
	const [colorScheme, setColorScheme] = useState<ColorScheme>('dark');
	const toggleColorScheme = (value?: ColorScheme) =>
		setColorScheme(value || (colorScheme === 'dark' ? 'light' : 'dark'));

	return (
		<>
			<ColorSchemeProvider
				colorScheme={colorScheme}
				toggleColorScheme={toggleColorScheme}
			>
				<MantineProvider
					theme={{ colorScheme }}
					withGlobalStyles
					withNormalizeCSS
				>
					<Container
						sx={{
							minHeight: '100dvh',
							height: '100%',
							width: '100%',
							position: 'relative',
						}}
						p="0"
						fluid
					>
						<HeaderMenu />

						<Flex
							id="flex-c"
							align="center"
							justify="center"
							sx={{ minHeight: 'calc(100dvh - 56px)',position: 'relative'}}
						>
							<BadgeCard />
						</Flex>
					</Container>
				</MantineProvider>
			</ColorSchemeProvider>
		</>
	);
}

